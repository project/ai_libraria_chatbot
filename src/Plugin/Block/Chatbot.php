<?php

namespace Drupal\ai_libraria_chatbot\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a AI Libraria Chatbot Block.
 *
 * @Block(
 *   id = "ai_libraria_chatbot",
 *   admin_label = @Translation("AI Libraria Chatbot"),
 * )
 */
class Chatbot extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('ai_libraria_chatbot.settings');
    $embed = $config->get('chatbot_embed_script');

    return [
      '#theme' => 'embed_script',
      '#embed' => $embed,
    ];
  }

}
