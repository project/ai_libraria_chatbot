<?php

namespace Drupal\ai_libraria_chatbot\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * This config form is used to get AI libraria chatbot embed script code.
 */
class ChatbotAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ai_libraria_chatbot';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ai_libraria_chatbot.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('ai_libraria_chatbot.settings');
    $form['embed_script'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Embed Script'),
      '#default_value' => $config->get('chatbot_embed_script'),
      '#required' => TRUE,
      '#description' => $this->t('Please create AI Libraria Embed Script <a href=":url">Here</a> and paste in the above field.', [':url' => 'https://libraria.ai/']),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('ai_libraria_chatbot.settings')
      ->set('chatbot_embed_script', $form_state->getValue('embed_script'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
