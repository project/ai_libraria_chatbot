INTRODUCTION
------------

This module provides the AI Libraria Chatbot.


INSTALLATION
------------

Enable this module from extend list page /admin/modules.


CONFIGURATION
-------------

1. Login to your https://libraria.ai/ account using of this link https://app.libraria.ai/signup.
2. Once logged in create your library. Add Knowledge base and Assistant Name and create the library.
3. Add Knowledge in the sidebar of your knowledge base name. Use any of the knowledge base Ex: Crawl website, Scrape with links, Scrape sitemap, Add unstructured documents or Add a file.
4. On the Dashboard, click chatbot sharing and add whitelist our domain url.
5. Click on knowledgebase settings and customoze the widget.(change color and widget) 
5. Copy the Embed Script code.
6. Embed the code on configuration path (/admin/config/chatbot/settings).
